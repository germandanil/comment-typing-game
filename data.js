export const texts = [
  "On the mountain, the monkey befriends various animals and joins a group of other wild monkeys. After playing, the monkeys regularly bathe in a stream.",
  "When the battle finally drew to a close, all were singed and frozen, soaked and cut and pierced. Rubick stood apart, sore but delighted in the week's festivities.",
  "The wind pitied the child and so lifted her into the sky and deposited her on a doorstep in a neighboring village."
];
const num = texts.length




export default { texts };
export {num}
