import {randomNumber} from "./index"
import { greetings,greetingsNum} from "../dataComments"
import {generalState,generalStateNum} from "../dataComments"

import {resultTimeUp,resultTimeUpNum} from "../dataComments"

import {BeforeTheFinish,BeforeTheFinishNum} from "../dataComments"

import {resultLastFinish,resultLastFinishNum}  from "../dataComments"

import {lastOnFinish,lastOnFinishNum} from "../dataComments"

import {onFinish,onFinishNum} from "../dataComments"

import {startCommentNum,startComment} from "../dataComments"

import {announcement,announcementNum} from "../dataComments"

import  {entertainment,entertainmentNum} from "../dataComments"

const numerationResult = ["первым" ,"вторым","третьим","четвертым","пятым","шестым","седьмым","восьмым","девятым","десятым"]
const numerationGeneral = ["первый" ,"второй","третий","четвертый","пятый","шестой","седьмой","восьмой","девятый","десятый"]
const history = new Map()


class EntertainmentComment{
    constructor(){
        const commentsID = randomNumber(entertainmentNum);
        this.comments = entertainment[commentsID];
    }
}



class WelcomeComment{
    constructor () {
        const commentID = randomNumber(greetingsNum);
        this.comments =greetings[commentID] ;
    }
}
class AnnouncementComment{
    constructor (list) {
        const commentID = randomNumber(announcementNum);
        let comment =announcement[commentID] + "И так";
        list.forEach( player =>{
                if(history.has(player)){
                    const playerHistory = history.get(player);
                    for(let i =0;i<5;i++)
                        if(playerHistory.has(i))
                            {
                                comment+= ", "+ player +" был "+playerHistory.get(i)+" раз " + numerationGeneral[i];
                                break;
                            }
                }
                else{
                    comment+=", "+player +" новый игрок";
                }
        })
        this.comments = comment;
    }
}
class StartComment{
    constructor () {
        const commentID = randomNumber(startCommentNum);
        this.comments =startComment[commentID] ;
    }
}
class GeneralStateComment{
    constructor ([list,progress]) { 
        const commentID = randomNumber(generalStateNum);
        let comment = generalState[commentID];
        for(let i =0;i<list.length;i++){
            if(i!=0)
                comment+=", ";
            comment+= list[i]+" ";
            comment+=numerationGeneral[i];
            if(progress.get(list[i]).length )
            comment+=" и проехать осталось еще " + progress.get(list[i]).length+" символов";
            else
            comment+= " уже устал ждать остальных на финише"
        }
        comment+="."
        this.comments = comment ;
    }
}
class BeforeTheFinishComment{
    constructor(name){
        const commentID = randomNumber(BeforeTheFinishNum);
        let comment = BeforeTheFinish[commentID];
        comment = name+comment;
        this.comments = comment;
    }
}
class onFinishComment{
    constructor(name){
        const commentID = randomNumber(onFinishNum);
        let comment = onFinish[commentID];
        comment = name+comment;
        this.comments = comment;
    }
}
class ResultTimeUpComment{
    constructor ([listWinner,listTime]) { 
        const commentID = randomNumber(resultTimeUpNum);
        let comment = resultTimeUp[commentID];
        for(let i =0;i<listWinner.length;i++){
            if(!history.has(listWinner[i]))
                history.set(listWinner[i],new Map())   
            if(i!=0)
                comment+=", ";
            const playerHistory = history.get(listWinner[i]);
            if(playerHistory.has(i))
                playerHistory.set(i,playerHistory.get(i)+1);
            else
                playerHistory.set(i,1);
            comment+= listWinner[i]+" стал ";
            comment+=numerationResult[i];
            const time = listTime.get(listWinner[i]);
            if(time>0)
                comment+=" со временем "+time;
            else
                comment+=" однако не доехав до финиша";
        }
        comment+="."
        this.comments = comment ;
    }
}
class ResultLastFinishComment{
    constructor ([listWinner,listTime]) { 
        const LastFinishCommentID = randomNumber(lastOnFinishNum);

        const commentID = randomNumber(resultLastFinishNum)
        let comment =listWinner[listWinner.length-1]+lastOnFinish[LastFinishCommentID] + resultLastFinish[commentID];
        for(let i =0;i<listWinner.length;i++){
            if(!history.has(listWinner[i]))
                history.set(listWinner[i],new Map())            
            if(i!=0)
                comment+=", ";
            const playerHistory = history.get(listWinner[i]);
            if(playerHistory.has(i))
                playerHistory.set(i,playerHistory.get(i)+1);
            else
                playerHistory.set(i,1);
            comment+= listWinner[i]+" стал ";
            comment+=numerationResult[i];
            const time = listTime.get(listWinner[i]);
            if(time>0)
                comment+=" со временем "+time;
            else
                comment+=" однако не дошев до финиша";
        }
        comment+="."
        this.comments = comment ;
    }
}
class CommentFactory{                                                      //factory pattern
    create (type,data = []) {
        let comment
        if (type === "welcome") 
            comment = new WelcomeComment();
        else if (type === "general") 
            comment = new GeneralStateComment(data);
        else if( type === "lastFinish")
            comment=  new ResultLastFinishComment(data)
        else if( type === "timeup")
            comment = new ResultTimeUpComment(data)
        else if( type === "beforeFinish")
            comment = new BeforeTheFinishComment(data);
        else if(type === "onFinish")
            comment = new onFinishComment(data);
        else if(type === "start")
            comment = new StartComment();
        else if(type === "announcement")
            comment = new AnnouncementComment(data);
        else if(type === "entertainment")
            comment = new EntertainmentComment();
        comment.type = type;
        return comment;    
    }
}
export {CommentFactory}