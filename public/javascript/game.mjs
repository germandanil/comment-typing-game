
import {proxiedFetch} from "./clearProxy.mjs"
import { commentator } from "./clearCommentator.mjs";
import {addClass} from "./helper.mjs"
import {removeClass} from "./helper.mjs"



const username = sessionStorage.getItem("username");
const socket = io("", {
    query: {
        username
    }
});

commentator(socket);

let TEXT = { text: "" }
let room = "waiting_room"



const readyBtn = document.getElementById("ready-btn");
const readyBtnAddClass = _.curry(addClass)(readyBtn)         //curring
const readyBtnRemoveClass = _.curry(removeClass)(readyBtn)  //curring

const textContainer = document.getElementById("text-container");
const textContainerAddClass = _.curry(addClass)(textContainer)  //curring
const textContainerRemoveClass = _.curry(removeClass)(textContainer)    //curring

const timer = document.getElementById("timer");
const timerAddClass = _.curry(addClass)(timer)  //curring   
const timerRemoveClass = _.curry(removeClass)(timer)    //curring

const addRoomBtn = document.getElementById("add-room-btn");

const quitRoomBtn = document.getElementById("quit-room-btn");
const quitRoomBtnAddClass = _.curry(addClass)(quitRoomBtn)  //curring
const quitRoomBtnRemoveClass = _.curry(removeClass)(quitRoomBtn)    //curring

const roomsPage = document.getElementById("rooms-page")
const roomsPageAddClass = _.curry(addClass)(roomsPage)  //curring
const roomsPageRemoveClass = _.curry(removeClass)(roomsPage)    //curring

const roomsName = document.getElementById("room-name")

const gamePage = document.getElementById("game-page")
const gamePageAddClass = _.curry(addClass)(gamePage)    //curring
const gamePageRemoveClass = _.curry(removeClass)(gamePage)  //curring


function doStartTextElemSetting(){
    quitRoomBtnAddClass("hidden");
    readyBtnAddClass("display-none");
    timerRemoveClass("display-none");
    timerAddClass("start-counter");
    timer.innerHTML = ""
}

function addLetterHandler(event) {
    socket.emit("PUT_LETTER", [room, event.key]);
}

function createGameText(textObj) {
    const textString = textObj.text
    textContainer.innerHTML = ""
    for (let i = 0; i < textString.length; i++) {
        let letter = textString.charAt(i);
        const span = document.createElement("span");
        if (i == 0)
            span.className = "next"
        else
            span.className = "unwritten";
        span.innerHTML = letter;
        textContainer.append(span);
    }
}

function createPlayer(playerInf) {
    const player_wrp = document.createElement("div");
    player_wrp.className = "player-wrp"

    const player = document.createElement("div");
    player.classList.add("player", "flex");

    const user_progress_wrp = document.createElement("div");
    user_progress_wrp.className = "user-progress-wrp"

    const circle = document.createElement("div");
    circle.classList.add("circle");
    circle.id = `status_${playerInf[0]}`
    if (playerInf[1])
        circle.classList.add("ready-status-green")
    else
        circle.classList.add("ready-status-red")
    const name = document.createElement("p");
    name.className = "name"
    name.innerHTML = playerInf[0];
    if (username == playerInf[0])
        name.innerHTML += " (you)"
    const bar = document.createElement("div");
    bar.classList.add("user-progress", playerInf[0])
    player_wrp.append(player, user_progress_wrp);
    player.append(circle, name);
    user_progress_wrp.append(bar);

    document.getElementById("players-list").append(player_wrp);

}

function createRoom(roomID, length) {
    const roomElem = document.createElement("div");
    roomElem.className = "room";
    const p = document.createElement("p");
    p.className = "count-in-room"
    p.innerHTML = `${length} user conected`;
    const h = document.createElement("h3");
    h.className = "room-name"
    h.innerHTML = roomID;
    const btn = document.createElement("button");
    btn.className = "join-btn"
    btn.id = `room_${roomID}`
    btn.innerHTML = "Join";
    roomElem.append(p, h, btn);
    document.getElementById("rooms").append(roomElem);
}

function preparation_for_login() {
    sessionStorage.clear();
    window.alert("This username is already taken!")
    window.location.replace("/login");
}
if (!username) {
    window.location.replace("/login");
}




addRoomBtn.addEventListener("click", () => {
    const result = window.prompt("Enter room name:");
    if (result) {
        socket.emit("ADD_NEW_ROOM", {
            userName: username,
            roomName: result
        });
    }
})
readyBtn.addEventListener("click", () => {
    console.log("?")
    const statusReady = document.getElementById(`status_${username}`);
    statusReady.className = "circle"
    if (readyBtn.className == "btn-ready-status-red") {
        readyBtnRemoveClass("btn-ready-status-red")
        readyBtnAddClass("btn-ready-status-green")
        readyBtn.innerHTML = "READY"
        statusReady.classList.add("ready-status-green");
    } else {
        readyBtnRemoveClass("btn-ready-status-green")
        readyBtnAddClass("btn-ready-status-red")
        readyBtn.innerHTML = "NOT READY"
        statusReady.classList.add("ready-status-red");
    }
    socket.emit("CHANGED_STATUS");
})
quitRoomBtn.addEventListener("click", () => {
    gamePageAddClass("display-none");
    roomsPageRemoveClass("display-none");
    socket.emit("LEAVE_ROOM");
    room = "waiting_room"
})





socket.on("MAIN_GAME_START", () => {
    timerRemoveClass("start-counter")
    timerAddClass("game-counter");
    textContainerRemoveClass("display-none");
    createGameText(TEXT);
    document.addEventListener("keydown", addLetterHandler)
})
socket.on("LETTER_APPROVED", () => {
    document.querySelector(".next").className = "written";
    if (document.querySelector(".unwritten"))
        document.querySelector(".unwritten").className = "next";
});
socket.on("UPDATE_BAR", playerInf => {
    const elem = document.querySelector(`.${playerInf[0]}`);
    elem.style.width = `${playerInf[1]*100}%`
    if (playerInf[1] == 1)
        elem.style.backgroundColor = "green";
})
socket.on("GAME_END", () => {
    timerAddClass("display-none");
    timerRemoveClass("game-counter")
    readyBtnRemoveClass("display-none btn-ready-status-green btn-ready-status-red")
    readyBtn.innerHTML = "NOT READY"
    quitRoomBtnRemoveClass("hidden")
    textContainer.innerHTML = ""
    textContainerAddClass("display-none")
    readyBtnAddClass("btn-ready-status-red")
    document.querySelectorAll("user-progress").forEach(elem => elem.style.width = "0px")

    
    document.removeEventListener("keydown", addLetterHandler);
})
socket.on("GAME_START", id => {
    proxiedFetch(TEXT,id);
    doStartTextElemSetting();
})
socket.on("UPDATE_TIMER", count => {
    timer.innerHTML = `${count}`
})


socket.on("CHANGE_PLAYER_STATUS", username => {
    const status_player = document.getElementById(`status_${username}`)
    if (status_player.classList.contains("ready-status-red")) {
        status_player.classList.remove("ready-status-red");
        status_player.classList.add("ready-status-green")
    } else {
        status_player.classList.remove("ready-status-green");
        status_player.classList.add("ready-status-red")
    }
})

socket.on("UPDATE_PLAYERS", players => {
    document.getElementById("players-list").innerHTML = ""
    players.forEach(player => {
        createPlayer(player);
    })
})


socket.on("ADD_TO_ROOM", infromation => {
    document.getElementById("players-list").innerHTML = ""
    roomsPageAddClass("display-none");
    gamePageRemoveClass("display-none")
    roomsName.innerHTML = infromation[0];
    room = infromation[0];
    infromation[1].forEach(player => {
        createPlayer(player);
    })
})
socket.on("USERNAME_ALREADY_TAKEN", preparation_for_login)
socket.on("UPDATE_ROOM", arrayRooms => {
    document.getElementById("rooms").innerHTML = "";
    arrayRooms.forEach(element => {
        createRoom(element[0], element[1].length)
    });
    document.querySelectorAll(".join-btn").forEach(btn => {
        btn.addEventListener("click", () => {
            socket.emit("JOIN_TO_ROOM", {
                userName: username,
                roomName: btn.id.substr(5)
            })
        })
    })

})