export function commentator(socket){
    const facadeComment = new CommentFacade();
    socket.on("GET_COMMENT",comment=>{
        facadeComment.addComment(comment)
    })
}
class CommentFacade{                                         //facade pattern
    addComment(comment){
        const commentTEXT = comment.comments;
        const commentGetter = new ElemComment();
        const commentElem = commentGetter.getElem("comment");
        const clearComment = new ClearComment();
        clearComment.clear(commentElem);

        const asyncComment = new AddAsyncComment();
        const staticComment = new AddStaticComment();
        const curryStaticAdd = _.curryRight(staticComment.add)(commentElem);        //curring
        const curryAsyncComment = _.curryRight(asyncComment.add)(commentElem);      //curring
        const type = comment.type;
        switch(type){
            case("welcome"):curryStaticAdd(commentTEXT); break;
            case("general"):curryAsyncComment(7,commentTEXT); break;
            case("timeup"):curryStaticAdd(commentTEXT); break;
            case("lastFinish"):curryStaticAdd(commentTEXT); break;
            case("onFinish"):curryAsyncComment(commentTEXT);break;
            case("start"):curryAsyncComment(5,commentTEXT);break;
            case("announcement"):curryStaticAdd(commentTEXT);break;
            case("beforeFinish"):curryAsyncComment(6,commentTEXT);break;
            case("entertainment"):curryAsyncComment(28,commentTEXT);break;
        }
    }
}



class AddAsyncComment{
    add(timeSec,comment,elem){
        elem.innerHTML = comment;
        setTimeout(()=>{
            if(elem.innerHTML==comment)
                elem.innerHTML = "";   
        }
        ,timeSec*1000)
    }

}

class AddStaticComment{
    add(comment,elem){
        elem.innerHTML = comment;
    }
}
class ClearComment{
    clear(elem){
        elem.innerHTML = "";
    }
}
class ElemComment{
    getElem(id){
        const commentElem = document.getElementById(id)
        return commentElem;
    }
}