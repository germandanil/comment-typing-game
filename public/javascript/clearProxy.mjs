
//pattern proxy
const cache = new Map()
function getURLforFetch(id){
    const host = window.location.hostname;
    let src = `${window.location.host}/game/texts/${id}`
    if(host == "localhost")
        src ="http://" + src;
    else src = "https://" + src;
    return src;
}
async function setTextFromFetch(textObj,id){
    const URL = getURLforFetch(id)
    const response = await fetch(URL);
    textObj.text = await response.text()

    cache.set(id,textObj.text);
}


const proxiedFetch = new Proxy(setTextFromFetch,{                 //proxy pattern
    apply(target, thisArg, args ){
        const textObj = args[0]
        const id = args[1]
        if( cache.has(id))
        {
            textObj.text = cache.get(id);
        }
        else{
            Reflect.apply(target,thisArg,args);
        }
    }
})
export {proxiedFetch}

